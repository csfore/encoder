# Encoder

This is just a little encoding program I made in rust. Mostly meant for
personal use but MRs and issues are welcome.

## Attributions

A lot of the work done here wouldn't be done without references to 
AtropineTears's work on the library [here][1]

[1]: https://github.com/AtropineTears/To-Binary

## Licenses

This is licensed under GPLv3