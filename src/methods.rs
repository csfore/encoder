pub trait StrEncoder {
    fn to_ascii(&self) -> Vec<u8>;
}

impl StrEncoder for str {
    fn to_ascii(&self) -> Vec<u8> {
        self.as_bytes().to_vec()
    }
}