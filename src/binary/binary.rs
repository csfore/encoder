use std::fmt;
use crate::decimal::decimal::Decimal;

#[derive(Debug)]
pub struct Binary(pub String);

impl From<i32> for Binary {
    fn from(int: i32) -> Self {
        let mut bin: Vec<i32> = vec![];
        let mut conv = int.clone();
        while conv != 0 {
            let quo = conv % 2;
            conv = conv / 2;
            bin.push(quo);
        }
        return Binary(bin.into_iter().map(|i| i.to_string()).collect::<String>());
    }
}

impl From<Vec<u8>> for Binary {
    fn from(bytes: Vec<u8>) -> Self {
        // I can't keep this here in good conscience without giving credit to AtropineTears
        // Without their prior work I would've made this 10x more complicated
        // Please refer to this line for where I got this from
        // https://github.com/AtropineTears/To-Binary/blob/master/src/lib.rs#L108
        let mut bin_string: String = String::new();

        for byte in bytes {
            bin_string.push_str(&format!("{:08b} ", byte));
        }
        return Binary(bin_string)
    }
}

impl From<Decimal> for Binary {
    fn from(bytes: Decimal) -> Self {
        return match bytes {
            Decimal(vector) => {
                let mut bin_string: String = String::new();

                for byte in vector {
                    bin_string.push_str(&format!("{:08b} ", byte));
                }
                Binary(bin_string)
            }
        }

    }
}

impl fmt::Display for Binary {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}