use crate::binary::binary::Binary;

mod methods;
// use crate::methods::IntEncoder;
mod binary;
mod decimal;
use crate::decimal::decimal::Decimal;

fn main() {
    let stri = Decimal::from(String::from("Hello"));
    println!("{:?}", stri);
    let y = Binary::from(stri);
    println!("{}", y);
}

