#[derive(Debug)]
pub struct Decimal(pub Vec<u8>);

impl From<String> for Decimal {
    fn from(text: String) -> Self {
        Decimal(text.as_bytes().to_vec())
    }
}

// impl From<String> for Decimal {
//     fn from(text: String) -> Vec<u8> {
//         let mut bytes_string = String::new();
//         let mut bytes_vec = text.as_bytes().to_vec();
//
//         for byte in bytes_vec {
//             bytes_string.push_str(&format!("{:?} ", byte));
//         }
//
//         Decimal(bytes_string)
//     }
// }

// impl fmt::Display for Decimal {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "{}", self)
//     }
// }